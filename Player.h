#pragma once
#include <SFML/Graphics.hpp>
#include "Map.h"

struct KeyEvevnts {
	//������/aliases  (using)
	//git commit -m
	using Key = sf::Keyboard::Key;
	Key key_up = Key::W;
	Key key_down = Key::S;
	Key key_left = Key::A;
	Key key_right = Key::D;
	Key key_action_1 = Key::Num0;
	Key key_action_2 = Key::Num1;
	Key key_action_3 = Key::Num2;
	Key key_action_4 = Key::Num3;
	Key key_action_5 = Key::Num4;
	Key key_action_6 = Key::Num5;
	Key key_action_7 = Key::Num6;
	Key key_action_8 = Key::Num7;
	Key key_action_9 = Key::Num8;
	Key key_action_10 = Key::Num9;
	Key key_action_11 = Key::Q;
	Key key_action_12 = Key::E;
	Key shift = Key::LShift;
};

class Player {
private:
	using position = float;
	sf::Texture texture;
	sf::Sprite sprite;
	position x;
	position y;
	float speed_player = 1.f;
	KeyEvevnts key_events;

public:
	Player(float x, float y);

	float get_speed();

	void set_speed(float new_speed);

	float get_x();

	float get_y();

	void set_texture_from_file(const char* pictures);

	void picture_control(Map* map,double multiplier);
		

		
	void draw(sf::RenderWindow& wnd);

};