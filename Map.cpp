#include "Map.h"

Map::Map()
{
	textures = new sf::Texture[TEXTURES];
	sprites = new sf::Sprite[TEXTURES];
	char texture_names[TEXTURES][SIZE_TEXTURES] = {
		"textures/top_or_dayn.jpg",
		"textures/wheat.jpg",
		"textures/Woter.jpg",
		"textures/Left_or_ride.jpg",
		"textures/grass.jpg",
		"textures/wood_planks.png",
		"textures/dirt.jpg",
		"textures/diamond.png",
		"textures/TNT.jpg",
		"textures/chestblock.png",
		"textures/stone.png",
		"textures/sand.png"
	};

	for (int i = 0; i < TEXTURES; i++) {
		textures[i].loadFromFile(texture_names[i]);
		sprites[i].setTexture(textures[i]);
	}
}

Map::~Map() {
	delete[] textures;
	delete[] sprites;
}

bool Map::set(int row, int column, int element) {
	if (row < 0) {
		return false;
	}
	else if (element < 0) {
		return false;
	}
	else if (column < 0 || column >= COLUMN) {//false || true -> true
		return false;
	}
	else {
		array_map[row][column] = element;
		return true;
	}
}

int Map::get(int row, int column)
{
	return array_map[row][column];
}

void Map::draw(sf::RenderWindow& wnd) {
	int size = 40;
	for (int i = 0; i < ROW; i++) {
		for (int v = 0; v < COLUMN; v++) {
			int n = array_map[i][v];
			sprites[n].setPosition((float)(v * size), (float)(i * size));
			wnd.draw(sprites[n]);
		}
	}
}