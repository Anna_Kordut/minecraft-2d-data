#include <iostream>
#include "Map.h"
#include "Player.h"
#include <chrono>
#include <thread>
using namespace sf;
using namespace std;
using namespace std::chrono;

//1. �������� ���� ������� ������� � �������������� snake case
//
//2. �������� ���� ���������� ������� � �������������� snake case
//
//3. �������� ���� �������, �������� � enum - �� ������� � �������������� pascal case
//
//4. �������� ���� �������� ������� ������� � �������������� screaming snake case
//
//5. ��� - �� ������ ����� �� � ����� �������.��: 0 ����� : 1
//
//	6. ��� - �� ������ ����� �� � ����� if.�� : 0 ����� : 1
//
//	8. ��� - �� ������ ����� �� � ����� while\for.��: 0 ����� : 1
//
//	9. ����� ������ ����������(+-= / ���) ������� ������ �� � �����.
//
//	10. �������� ���� ���������� ���������� ������ ������������� ������ : 
//  
//  11. �������� ���� �������� ������ ������������� ������ : 
//
//  12. �������� ���� ��������� ������� ������ ������ ������������� ������ :  

int main() {
	setlocale(LC_ALL, "rus");
	VideoMode video(450, 450);
	RenderWindow wnd(video, "Game");
	Event event;
	Map map;
	Player plaer(350.f, 200.f);
	plaer.set_texture_from_file("textures/fish.png");
	plaer.set_speed(4.f);



	double multiplier = 1.0;
	while (wnd.isOpen() == true)
	{
		auto begin = high_resolution_clock::now();

		//system("cls");		
		//cout << "X: " << plaer.get_x() << endl;
		//cout << "Y: " << plaer.get_y() << endl;
		//cout << "������ ������ �� �������: " << int(plaer.get_x()/40.f) << endl;
		//cout << "������ ������ �� ������: " << int(plaer.get_y()/40.f) << endl;
		//std::cout << plaer.get_speed()<<std::endl;
		while (wnd.pollEvent(event))
		{
			if (event.type == Event::Closed) {
				wnd.close();
			}
		}

		std::this_thread::sleep_for(50ms);
		wnd.clear(Color::White);
		map.draw(wnd);
		plaer.picture_control(&map, multiplier);
		plaer.draw(wnd);
		wnd.display();
		auto end = high_resolution_clock::now();
		auto duration = duration_cast<chrono::microseconds>(end - begin);
		long long count = duration.count();
		multiplier = (double)count / 10000;

	}
	return 0;
}