#include "Player.h"
#include "Map.h"

Player::Player(float x, float y) {
	speed_player = 0.f;
	this->x = x;
	this->y = y;
}

float Player::get_speed() {
	return speed_player;
}

void Player::set_speed(float new_speed) {
	if (new_speed < 0.f) {
		speed_player = 0.f;
		return;
	}
	speed_player = new_speed;
}

float Player::get_x() {
	return x;
}

float Player::get_y() {
	return y;
}

void Player::set_texture_from_file(const char* pictures) {
	texture.loadFromFile(pictures);
	sprite.setTexture(texture);
}
void explosion_tnt(Map* map,float y_pos,float x_pos) {
	map->set(int(y_pos), int(x_pos), 6);
	
	if (map->get(int(y_pos) + 1, int(x_pos)) == 8)
		explosion_tnt(map, y_pos + 1, x_pos);
	else
		map->set(int(y_pos + 1), int(x_pos), 6);

	if (map->get(int(y_pos) - 1, int(x_pos)) == 8)
		explosion_tnt(map, y_pos - 1, x_pos);
	else
		map->set(int(y_pos) - 1, int(x_pos), 6);


	if (map->get(int(y_pos), int(x_pos) - 1) == 8)
		explosion_tnt(map, y_pos, x_pos+1);
	else
		map->set(int(y_pos), int(x_pos) - 1, 6);


	if (map->get(int(y_pos), int(x_pos) - 1) == 8)
		explosion_tnt(map, y_pos, x_pos-1);
	else
		map->set(int(y_pos), int(x_pos)-1, 6);



}
void Player::picture_control(Map* map,double multiplier) {
	int y_pos = (int)(y) / 40; 
	int x_pos = (int)(x) / 40;
	float speed = speed_player;
	if (map->get(y_pos, x_pos)==2) {
		speed /= 8.f;

	}
	if (map->get(y_pos, x_pos) == 5) {
		speed /= 12.f;

	}
	if (map->get(y_pos, x_pos) == 8) {
		explosion_tnt(map, float(y_pos), float(x_pos));
	

	}

	if (sf::Keyboard::isKeyPressed(key_events.key_action_1)) {
		map->set(y_pos, x_pos, 0);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_2)) {
		map->set(y_pos, x_pos, 1);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_3)) {
		map->set(y_pos, x_pos, 2);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_4)) {
		map->set(y_pos, x_pos, 3);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_5)) {
		map->set(y_pos, x_pos, 4);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_6)) {
		map->set(y_pos, x_pos, 5);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_7)) {
		map->set(y_pos, x_pos, 6);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_8)) {
		map->set(y_pos, x_pos, 7);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_9)) {
		map->set(y_pos, x_pos, 8);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_10)) {
		map->set(y_pos, x_pos, 9);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_11)) {
		map->set(y_pos, x_pos, 10);
	}
	else if (sf::Keyboard::isKeyPressed(key_events.key_action_12)) {
		map->set(y_pos, x_pos, 11);
	} 
	if (sf::Keyboard::isKeyPressed(key_events.shift)) {
		speed *= 1.5f * float(multiplier);
	}

	if (sf::Keyboard::isKeyPressed(key_events.key_right)) {
		if (450.f - 40.f >= x)
			x += speed * float(multiplier);
	}

	if (sf::Keyboard::isKeyPressed(key_events.key_left)) {
		if (0.f < x)
			x -= speed * float(multiplier);
	}

	if (sf::Keyboard::isKeyPressed(key_events.key_up)) {
		if (0.f < y)
			y -= speed * float(multiplier);
	}

	if (sf::Keyboard::isKeyPressed(key_events.key_down)) {
		if (410.f > y)
			y += speed * float(multiplier);
	}

	
}

void Player::draw(sf::RenderWindow& wnd) {
	sprite.setPosition(x, y);
	wnd.draw(sprite);
}
