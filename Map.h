#pragma once
#include <SFML/Graphics.hpp>

#define TEXTURES 12
#define SIZE_TEXTURES 256
#define COLUMN 12
#define ROW 12

class Map {
private:
	sf::Texture* textures;//1
	sf::Sprite* sprites;//2
	int array_map[ROW][COLUMN] = {
		//0  1  2  3  4  5  6  7  8  9
		{ 0, 1, 2, 3, 4, 5, 6, 6, 6, 6,2,10},//0
		{ 6, 4, 4, 4, 4, 4, 9, 10, 4, 6,2,11},//1
		{ 6, 4, 2, 2, 2, 2, 8, 8, 4, 6,2,2},//2
		{ 6, 4, 2, 2, 2, 2, 2, 8, 4, 6,2,2},//3
		{ 6, 4, 2, 11,11,2, 2, 8, 4, 6,2,2},//4
		{ 6, 4, 2, 2, 2, 2, 2, 8, 4, 6,2,2},//5
		{ 6, 4, 9, 2, 2, 2, 9, 8, 4, 6,2,2},//6
		{ 10, 4, 4, 4, 5, 5, 4, 4, 4, 6,2,2},//7 
		{ 10, 4, 4, 4, 5, 5, 4, 4, 4, 6,2,2},//8
		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,2,2},//9
		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,2,2},//10
		{ 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,2,2}//11
	};
public:
	Map();

	~Map();

	bool set(int row, int column, int element);

	int get(int row, int column);

	void draw(sf::RenderWindow& wnd);
};